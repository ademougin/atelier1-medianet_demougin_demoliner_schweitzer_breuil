# MEDIANET - Rapport conception 
###### Clara De Moliner - Alban Demougin - Fabien Breuil - Victorien Schweitzer

## Rendus
#### Conception
- Envoyé sur arche : archive contenant le dossier de conception :
	- dossier wireframes
	- dossier scenarios
	- dossier diagrammes
- [Balsamiq -> Wireframes, avec navigation dans les liens](https://iutnc-info.mybalsamiq.com/projects/lpcisiieat1nancyphoneurs/grid)

#### Sources 
- [Depot BitBucket](https://bitbucket.org/ademougin/atelier1-medianet_demougin_demoliner_schweitzer_breuil)

####Site déployé 
- [Site adhérent](https://webetu.iutnc.univ-lorraine.fr/www/breuil1u/sourcesAdherent/public/)
- [Site administrateur](https://webetu.iutnc.univ-lorraine.fr/www/breuil1u/sourcesAdmin/public/login)

##Installation et utilisation
- installer la BDD depuis le dossier sources/scripts
- configurer les fichiers config.ini (en les recopiant depuis _config.ini) dans les repertoires sources/sourcesAdmin(et sourcesAdherent)/app afin d'effectuer la connexion à la BDD dans sourcesAdmin et dans sourcesAdherent
- ne pas oublier de faire _composer update_ pour les deux applications
- les applications sont utilisables dans [webetu](https://webetu.iutnc.univ-lorraine.fr/www/breuil1u/) en accedant aux dossiers public/ des repertoires sources/sourcesAdmin(ou adhérent)
- Un id admin par défaut : admin@mail.com / admin
- Un id adherent par défaut : adherent@mail.com / adherent

##Travail réalisé 
### Fonctionnalités communes
* Outils de sécurité SecurityTools (cryptage MDP, sécurisation des saisies... )

#### Adhérent 
* Connexion
* Réserver un document -> aller sur la fiche du doc et le reserver, apres s'être connecté
* Consulter ses emprunts -> se connecter et cliquer sur consulter ses emprunts
* Rechercher un document utiliser _soit_ la recherche rapide dans toutes les pages, _soit_ la recherche avancée, en cliquand sur son lien.
* Afficher une liste de documents -> page d'accueil, ou recherche
* Afficher les détails d'un documents->clique sur son titre
* Mise en page, CSS, installation de la grille

### Staff
* Connexion
* Ajoute / Rechercher / Modifier / Supprimer un adhérent -> rubrique gestion adherent
* Ajouter un document -> gestion fond
* Rechercher / Editer / Supprimer un document -> gestion fond
* Ajouter un genre / type / état -> gestion fond
* Emprunter un document -> pret
* Afficher le récapitulatif d'un emprunt -> après un pret
* Rendre un document -> Retours document
* Afficher le récapitulatif d'un retour avec documents restants à rendre
* Effectuer une reservation
* Securiser un emprunt sur une reservation
* Mise en page, CSS, installation de la grille