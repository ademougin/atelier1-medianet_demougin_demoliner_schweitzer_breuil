<?php
$auth = new Authentification($app);


//renvoie a l'accueil
$app->get('/', function () use ($app) {
    $c = new Controller($app);
    $c->accueil();
})->name('root');

//MODULES
$app->get('/afficheConfirmation', function () use ($app) {
    $c = new Controller($app);
    $c->afficheConfirmation();
})->name('afficheConfirmation');

$app->get('/afficheErreur', function () use ($app) {
    $c = new Controller($app);
    $c->afficheErreur();
})->name('afficheErreur');

/*Moteur de Recherche avancee*/
$app->get('/rechercheAvancee', function () use ($app) {
    $c = new Controller($app);
    $c->afficheRechercheAvancee();
})->name('rechercheAvancee');


//AFFICHER DES PAGES
/*Accueil*/
$app->get('/accueil', function () use ($app) {
    $c = new Controller($app);
    $c->accueil();
})->name('accueil');

/*Affiche le détail d'un document*/
$app->get('/detailsDoc:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->afficherDetailsDocument($id);
})->name('detDocument');

/* Page de connexion */
$app->get('/login', function () use ($app) {
    $c = new Controller($app);
    $c->afficheLogin();
})->name('login');

/* Page d'affichage des emprunts d'un adhérent */
$app->get('/mesEmprunts', $auth, function () use ($app) {
    $c = new Controller($app);
    $c->afficheMesEmprunts();
})->name('mesEmprunts');

//MECANIQUE
///*effectuer une recherche*/
$app->get('/effectuerRecherche', function () use ($app) {
    $c = new Controller($app);
    $c->effectuerRecherche();
})->name('resRecherche');

/*reserver un document*/
$app->get('/reserverDocument:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->effectuerReservation($id);
})->name('resReserver');

/* Valide les informations saisie lors du login */
$app->post('/valider-login', function () use ($app) {
    $c = new Controller($app);
    $c->login();
})->name('valider-login');

/* Decconecte l'adherent */
$app->get('/logout', $auth, function () use ($app) {
    $c = new Controller($app);
    $c->logout();
})->name('logout');