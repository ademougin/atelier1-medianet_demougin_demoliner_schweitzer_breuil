<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etat_doc extends Eloquent
{
    protected $table = 'etat_doc';
    protected $primaryKey = 'id_etat_doc';
    public $timestamps = false;

    public function document(){
      return $this->hasMany('document', 'id_etat_doc');
    }

}
