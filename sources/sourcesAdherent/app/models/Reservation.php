<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Reservation extends Eloquent
{
    protected $primaryKey ='id_reservation';
    protected $table = 'reservation';
    public $timestamps = true;

    public function adherent() {
        return $this->belongsTo('Adherent', 'id_adherent');
    }
    public function document() {
        return $this->belongsTo('Document', 'id_document');
    }

}
