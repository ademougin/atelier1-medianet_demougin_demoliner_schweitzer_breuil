<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pret extends Eloquent
{
    protected $primaryKey ='id_pret';
    protected $table = 'pret';
    public $timestamps = true;

    public function adherent() {
        return $this->belongsTo('Adherent', 'id_adherent');
    }
    public function document() {
        return $this->belongsToMany('Document', 'pret_document','id_pret','id_document')->withPivot('date_retour_reelle');
    }
}
