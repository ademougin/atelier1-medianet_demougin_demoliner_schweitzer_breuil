<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Staff extends Eloquent
{
    protected $primaryKey ='id_staff';
    protected $table = 'staff';
    public $timestamps = true;
}
