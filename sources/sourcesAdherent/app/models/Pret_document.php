<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pret_document extends Eloquent
{
    protected $table = 'pret_document';
    public $timestamps = false;
}
