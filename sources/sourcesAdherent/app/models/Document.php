<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Document extends Eloquent
{
    protected $table = 'document';
    protected $primaryKey = 'id_document';
    public $timestamps = true;

    public function etat_doc()
    {
        return $this->belongsTo('Etat_doc', 'id_etat_doc');
    }

    public function genre_doc()
    {
        return $this->belongsTo('Genre_doc', 'id_genre_doc');
    }

    public function type_doc()
    {
        return $this->belongsTo('Type_doc', 'id_type_doc');
    }

    public function pret()
    {
        return $this->belongsToMany('Pret', 'pret_document', 'id_document', 'id_pret')->withPivot('date_retour_reelle');
    }

    public function reservation()
    {
        return $this->hasMany('Reservation', 'id_document');
    }

    //récupère tous les documents
    public static function getAllDocuments()
    {
        return Document::orderBy('created_at', 'DESC')
            ->with('etat_doc', 'genre_doc', 'type_doc')
            ->get();
    }

    //récupère le details d'un doc par son id
    public static function getDetailsDocument($id)
    {
        $doc = Document::with('etat_doc', 'genre_doc', 'type_doc', 'pret')
            ->find($id);
        return $doc;
    }

    //Retourne si un document est emprunté ou non
    public static function estEmprunte($id)
    {
        $doc = Document:: with('etat_doc')->find($id);
        if ($doc->etat_doc->id_etat_doc == 2) {
            return true;
        } else {
            return false;
        }
    }

    public static function dateRetour($id)
    {
        $doc = Document::with('pret')->find($id);
        return $doc->date_retour_limite;
    }

    public static function reserverDocument($id, $idadherent){
        $doc = Document::with('pret','etat_doc','reservation')->find($id);

        if ($doc->id_etat_doc == 2){
        $doc->id_etat_doc = 6;
        } else {
            $doc->id_etat_doc = 5;
        }

        $reservation = new Reservation();
        $reservation->id_adherent = $idadherent;
        $reservation->id_document = $id;

        $doc->save();
        $reservation->save();
    }

}
