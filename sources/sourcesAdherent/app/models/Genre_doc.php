<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre_doc extends Eloquent
{
    protected $table = 'genre_doc';
    protected $primaryKey = 'id_genre_doc';
    public $timestamps = false;


    public function document(){
      return $this->hasMany('Document', 'id_document');
    }

}
