<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Type_doc extends Eloquent
{
    protected $table = 'type_doc';
    protected $primaryKey = 'id_type_doc';
    public $timestamps = false;


    public function document(){
        return $this->hasMany('Document', 'id_document');
    }

}
