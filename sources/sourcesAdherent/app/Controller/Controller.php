﻿<?php

class Controller
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    /*MODULES*/
    //AFFICHER LE HEADER
    public function header()
    {
        $this->app->render('header.twig', array('isLogged' => SecurityTools::isLogged()));
    }

    //AFFICHER LE FOOTER
    public function footer()
    {
        $this->app->render('footer.twig');
    }

    //AFFICHER LA LISTE DE DOCUMENTS EN PARAMETRES AVEC QUELQUES ATTRIBUTS
    public function listeDocuments($documents)
    {
        $this->app->render('afficherlistedocuments.twig', array('documents' => $documents));
    }

    //AFFICHE LE MOTEUR DE RECHERCHE
    public function moteurRecherche()
    {
        $prets = Pret_document::All();
        $etats = Etat_doc::All();
        $genres = Genre_doc::All();
        $types = Type_doc::All();
        $this->app->render('moteurRecherche.twig', array(
            'prets' => $prets,
            'etats' => $etats,
            'genres' => $genres,
            'types' => $types
        ));
    }

    //Affiche un message de resultats de recherche
    public function info_resultat_recherche()
    {
        $this->app->render('info_resultat_recherche.twig');
    }

    //Fonction qui affiche un message de succès
    //  Si le paramètre $bool n'est pas renseigné alors par défaut on affiche pas le header et la sideBar
    //  Si le paramètre $nbBack n'est pas renseigné alors par défaut le nombre
    //    de page de retour sera de 1
    public function afficheConfirmation($message = '', $bool = false, $nbBack = 1)
    {
        if ($bool == true) {
            $this->header();
        }
        $this->app->render('info_action_confirme.twig', array(
            'message' => $message,
            'nbBack' => $nbBack
        ));
        if ($bool == true) {
            $this->footer();
        }
    }

    //Fonction qui affiche un message d'erreur
    //  Si le paramètre $bool n'est pas renseigné alors par défaut on affiche pas le header et la sideBar
    //  Si le paramètre $nbBack n'est pas renseigné alors par défaut le nombre
    //    de page de retour sera de 1
    public function afficheErreur($erreur = '', $bool = false, $nbBack = 1)
    {
        if ($bool == true) {
            $this->header();
        }
        $this->app->render('info_erreur.twig', array(
            'erreur' => $erreur,
            'nbBack' => $nbBack
        ));
        if ($bool == true) {
            $this->footer();
        }
    }

    //AFFICHE LES DETAILS D UN DOCUMENT
    public function detailsDoc($id)
    {
        $document = Document::getDetailsDocument($id); //recupere les details
        $isemprunt = Document::estEmprunte($id); //recuperer s'il est emprunté
        $this->app->render('details_doc.twig', array('document' => $document, 'emprunt' => $isemprunt));
    }

    // Affiche la page de connexion d'un adherent
    public function afficheLogin()
    {
        $this->header();
        $this->app->render('affiche_login.twig');
        $this->footer();
    }

    public function back()
    {
        $this->app->render('back_button.twig');
    }

    /*PAGES COMPLETES*/
    //GENERE LA PAGE D ACCUEIL
    public function accueil()
    {
        $this->header();
        $documents = Document::getAllDocuments();
        $this->listeDocuments($documents);
        $this->footer();
    }

    //GENERE LA PAGE DE RECHERCHE AVANCEE VIERGE
    public function afficheRechercheAvancee()
    {
        $this->header();
        $this->moteurRecherche();
        $this->footer();
    }

    //GENERE LA PAGE QUI AFFICHE UN DOCUMENT EN DETAIL
    public function afficherDetailsDocument($id)
    {
        $this->header();
        $this->detailsDoc($id);
        $this->back();
        $this->footer();
    }

    //GENERE LA PAGE POUR DE RESULTATS DE RECHERCHE
    public function afficherResultatRecherche($result)
    {
        $this->header();
        $this->moteurRecherche();
        //si resultats alors on les affiche
        if (count($result) > 0) {
            $this->info_resultat_recherche();
            $this->listeDocuments($result);
        } // si pas de resultats
        else {
            $this->afficheErreur("Aucun document trouvé.");
        }
        $this->footer();
    }


    /*MECANIQUE*/
    public function effectuerRecherche()
    {
        //LECTURE DES CHAMPS SAISIS
        $keyword = SecurityTools::securiseString($this->app->request->get('keyword'));
        $genre = SecurityTools::securiseInt($this->app->request->get('genre_doc'));
        $type = SecurityTools::securiseInt($this->app->request->get('type_doc'));
//        $keyword = strtolower($keyword);

        //CREATION DE LA REQUETE
        //select * from documents
        $query = Document::orderBy('created_at', 'DESC')
            ->with('etat_doc', 'genre_doc', 'type_doc', 'pret', 'reservation');

        //titre
        //si titre uniquement
        if ((strlen($keyword) > 0)) {
            if ($this->app->request->get('title_only')) {
                $query->where('titre', 'LIKE', '%' . $keyword . '%');
            } //si titre + description
            else {
                $query->where(function ($query) use ($keyword) {
                    $query->where('titre', 'LIKE', "%$keyword%");
                    $query->orWhere('description', 'LIKE', "%$keyword%");
                    $query->get();
                });
            }
        }

        //disponible
        if ($this->app->request->get('dispo_only')) {
            $query->where('id_etat_doc', '=', 1);
        }

        //genre
        if ($genre > 0) {
            $query->where('id_genre_doc', '=', $genre);
        }

        //type
        if ($type > 0) {
            $query->where('id_type_doc', '=', $type);
        }


        //EXECUTION DE LA REQUETE
        $result = $query->get();

        //RENDU VISUEL
        $this->afficherResultatRecherche($result);
    }

    //réservation
    public function effectuerReservation($id)
    {
        //on récupère le doc
        $doc = Document::with('etat_doc', 'pret')->find($id);

        //on sécurise avec une variable (1 réservation ok, 2 erreur de log, 3 autre erreur)
        $result = 3;

        $idetat = $doc->id_etat_doc;
        if (count($doc->pret) > 0) {
            $datepret = date_create($doc->pret[sizeof($doc->pret) - 1]->date_retour_limite);
        }
        //test si réservable
        if ($idetat == 1 && SecurityTools::isLogged() == true) {
            //on recupère l'id de l'adherent connecté
            $idadherent = $_SESSION['adherent'];
            $message = "Vous pouvez aller retirer votre document à la médiathèque";
            $result = 1;
        } elseif ($idetat == 2 && SecurityTools::isLogged() == true) {
            //on recupère l'id de l'adherent connecté
            $idadherent = $_SESSION['adherent'];
            $message = "Vous pourrez aller retirer votre document à partir du : " . $datepret->format('d/m/Y');
            $result = 1;
        } elseif (SecurityTools::isLogged() == false) {
            $erreur = "erreur, veuillez vous connecter";
            $result = 2;
        } else {
            $erreur = "Votre réservation n'a pas pu être effectuée";
        }

        //réserve le document si connecté
        if ($result == 1) {
            Document::reserverDocument($id, $idadherent);
        }
        //rendu utilisateur
        if ($result == 1) {
            $this->header();
            $this->afficheConfirmation($message, false, 2);
            $this->footer();
        } elseif ($result == 2) {
            $this->app->redirect($this->app->urlFor('login'));
        } else {
            $this->header();
            $this->afficheErreur($erreur, false, 2);
            $this->footer();
        }
    }


    public function login()
    {
        $this->header();
        $email = $this->app->request->post('adherent-email');
        $password = SecurityTools::SaltSha1Crypt($this->app->request->post('password'));
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $adherent = Adherent::where("email", "=", $email)->first();
            if ($adherent != null && $password === $adherent->mot_passe) {
                $_SESSION["adherent"] = $adherent->id_adherent;
                $this->app->redirect($this->app->urlFor('accueil'));
            } else {
                $erreur = "Email ou mot de passe invalide";
            }
        } else {
            $erreur = "Format d'email invalide";
        }
        $this->afficheErreur($erreur);
        $this->footer();
        exit();
    }


    public function logout()
    {
        session_destroy();
        $this->app->redirect($this->app->urlFor('accueil'));
    }

    public function afficheMesEmprunts()
    {
        $adherent = Adherent::getPretsEnCours($_SESSION['adherent']);
        $this->header();
        $this->app->render('mesEmprunts.twig', array('adherent' => $adherent));
        $this->footer();
    }
}
