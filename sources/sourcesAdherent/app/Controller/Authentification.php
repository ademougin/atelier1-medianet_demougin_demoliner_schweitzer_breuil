<?php

class Authentification extends Controller {
    public function __invoke($route)
    {
        if (SecurityTools::isLogged() === false)
        {
            $this->app->redirect($this->app->urlFor('login'));
        }
    }
}