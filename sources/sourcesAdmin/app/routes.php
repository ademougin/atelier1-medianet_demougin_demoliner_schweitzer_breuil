<?php
$auth = new Authentification($app);


/*DIVERS*/
// l'accueil est la page de retour d'un document pour le staff
$app->get('/', $auth, function () use ($app) {
    $app->redirect($app->urlFor('retourEmprunt'));
})->name('root');

// Route pour afficher une confirmation
$app->get('/afficheConfirmation', $auth, function () use ($app) {
    $c = new Controller($app);
    $c->afficheConfirmation();
})->name('afficheConfirmation');

// Route pour afficher une erreur
$app->get('/afficheErreur', function () use ($app) {
    $c = new Controller($app);
    $c->afficheErreur();
})->name('afficheErreur');

/* LOGIN */
// Route de la page de login
$app->get('/login', function () use ($app) {
    $c = new Controller($app);
    $c->afficheLogin();
})->name('login');

// Route de validation du login
$app->post('/valider-login', function () use ($app) {
    $c = new Controller($app);
    $c->login();
})->name('valider-login');

// Route de deconnexion
$app->get('/logout', $auth, function () use ($app) {
    $c = new Controller($app);
    $c->logout();
})->name('logout');



/*EMPRUNTS*/
// Route d'affichage de la page d'emprunt d'un document
$app->get('/afficheEmprunt', $auth, function () use ($app) {
    $c = new ControllerEmprunt($app);
    $c->afficheEmprunt();
})->name('afficheEmprunt');

// Route d'ajout d'un emprunt dans la base de données
$app->post('/ajoutEmprunt', $auth, function () use ($app) {
    $c = new ControllerEmprunt($app);
    $c->ajoutEmprunt();
})->name('ajoutEmprunt');


/*RETOURS*/
// Route pour afficher la page retour d'un emprunt
$app->get('/retourEmprunt', $auth, function () use ($app) {
    $c = new ControllerRetour($app);
    $c->retourEmprunt();
})->name('retourEmprunt');

// Route d'enregistrement du retour de l'emprunt
$app->post('/validerRetourEmprunt', $auth, function () use ($app) {
    $c = new ControllerRetour($app);
    $c->validerRetourEmprunt();
})->name('validerRetourEmprunt');


/*ADHERENTS*/
$app->get('/gestionAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->gestionAdherent();
})->name('gestionAdherent');

$app->get('/rechercheAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->rechercheAdherent();
})->name('rechercheAdherent');

$app->post('/listeAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->listeAdherent();
})->name('listeAdherent');

$app->get('/ajouterAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->ajoutAdherent();
})->name('ajouterAdherent');

$app->get('/modifierAdherent:id', $auth, function ($id) use ($app) {
    $c = new ControllerAdherent($app);
    $c->modifierAdherent($id);
})->name('modifierAdherent');

$app->post('/enregistrerAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->enregistrerAdherent();
})->name('enregistrerAdherent');

$app->post('/majAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->majAdherent();
})->name('majAdherent');

$app->get('/supprimerAdherent', $auth, function () use ($app) {
    $c = new ControllerAdherent($app);
    $c->supressionAdherent();
})->name('supprimerAdherent');

$app->get('/validerSuppressionAdherent', $auth, function() use($app) {
    $c = new ControllerAdherent($app);
    $c->validerSuppressionAdherent();
})->name('validerSuppressionAdherent');


/*FOND*/
//gerer fond
$app->get('/gestionFond', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->gestionFond();
})->name('gestionFond');

$app->get('/ajouterDocument', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->ajouterDocument();
})->name('ajouterDocument');

$app->post('/validerAjoutDoc', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->validerAjoutDoc();
})->name('validerAjoutDoc');

$app->get('/ajouterTypeGenreEtat', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->ajouterTypeGenreEtat();
})->name('ajouterTypeGenreEtat');

$app->post('/validerAjoutType', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->validerAjoutType();
})->name('validerAjoutType');

$app->post('/validerAjoutEtat', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->validerAjoutEtat();
})->name('validerAjoutEtat');

$app->post('/validerAjoutGenre', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->validerAjoutGenre();
})->name('validerAjoutGenre');


//editer document
$app->get('/effectuerRechercheDocument', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->effectuerRechercheDocument();
})->name('effectuerRechercheDocument');

$app->get('/rechercherEditerDocument', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->afficheRechercherEditerDocument();
})->name('rechercherEditerDocument');

$app->post('/validerEditionDoc', $auth, function () use ($app) {
    $c = new ControllerFond($app);
    $c->validerEditionDoc();
})->name('validerEditionDoc');

$app->get('/editionDoc:id', $auth, function ($id) use ($app) {
    $c = new ControllerFond($app);
    $c->afficherDetailsDocument($id);
})->name('editionDoc');

$app->get('/supprimerDocument:id', $auth, function ($id) use ($app) {
    $c = new ControllerFond($app);
    $c->supprimerDocument($id);
})->name('supprimerDocument');
