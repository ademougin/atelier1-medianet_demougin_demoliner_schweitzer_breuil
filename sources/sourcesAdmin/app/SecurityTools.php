<?php

class SecurityTools
{
    //pour les MPD : on couple avec un grain de sel et on crypte le tout
    public static function SaltSha1Crypt($mdpClair)
    {
        return sha1(sha1($mdpClair) . "$2y$07");
    }

    //si les valeurs entrée sont négatives on remet à 0
    public static function forcerPositive($in)
    {
        if ($in < 0) {
            return 0;
        } else return $in;
    }

    public static function isLogged()
    {
        if (isset($_SESSION['admin']) == true) {
            return true;
        } else {
            return false;
        }
    }

    public static function IsNullOrEmptyString($question)
    {
        return (!isset($question) || trim($question) === '');
    }

    public static function securiseString($in)
    {
        return trim(filter_var($in, FILTER_SANITIZE_STRING));
    }

    public static function securiseInt($in)
    {
        return trim(filter_var($in, FILTER_SANITIZE_NUMBER_INT));
    }

    public static function securiseUrl($in)
    {
        return trim(filter_var($in, FILTER_SANITIZE_URL));
    }

    public static function securiseEmail($in)
    {
        return trim(filter_var($in, FILTER_SANITIZE_EMAIL));
    }
}

