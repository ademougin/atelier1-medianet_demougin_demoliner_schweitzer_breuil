<?php

class ControllerRetour extends Controller
{
    //Module d'affichage du retour d'emprunt
    public function retourEmprunt()
    {
        $this->affichageInterface();
        $action = $this->app->urlFor("validerRetourEmprunt");
        $this->app->render('enregistrer_retour.twig', array(
            'action' => $action
        ));
        $this->footer();
    }

    //Enregistrer les documents qui sont rendus
    public function validerRetourEmprunt()
    {
        $this->affichageInterface();
        $messageErreur = "";

        if ($this->app->request->post('reference_document') != null) {

            $list_document = explode(';', $this->app->request->post('reference_document'));

            for ($i = 0; $i < sizeof($list_document); $i++) {
                try {
                    $document = Document::with('Pret')->find($list_document[$i]);
                    if ($document != null) {
                        if ($document->id_etat_doc == 2 || $document->id_etat_doc == 6) {
                            if ($document->id_etat_doc == 2) {
                                $document->id_etat_doc = 1;
                            } elseif ($document->id_etat_doc == 6) {
                                $document->id_etat_doc = 5;
                            }
                            $dateRetourReelle = date('Y-m-d H:i:s');
                            $pret_document = Pret_document::where('id_document', '=', $document->id_document)
                                ->whereNull('date_retour_reelle')
                                ->first();
                            $pret_document->date_retour_reelle = $dateRetourReelle;
                            
                            $pret_document->save();
                            $document->save();

                        
                            $adherent = Adherent::find($document->pret[sizeof($document->pret) - 1]->id_adherent);
                            $dateLimite = $document->pret[sizeof($document->pret)-1]->date_retour_limite;
                            if ($dateRetourReelle > $dateLimite) {
                                $adherent->nb_retard = $adherent->nb_retard + 1;
                                $adherent->save();
                            }

                        } elseif ($document->id_etat_doc == 1) {
                            $messageErreur .= "Le document " . $document->titre . "(" . $document->id_document . ") n'est pas preté. Les autres documents éventuels ont bien été restitués.<br /><br />";
                        } elseif ($document->id_etat_doc == 3) {
                            $messageErreur .= "Le document " . $document->titre . "(" . $document->id_document . ") a été perdu.Les autres documents éventuels ont bien été restitués.<br /><br />";
                        } elseif ($document->id_etat_doc == 4) {
                            $messageErreur .= "Le document " . $document->titre . "(" . $document->id_document . ") est en réparation.Les autres documents éventuels ont bien été restitués.<br /><br />";
                        } elseif ($document->id_etat_doc == 5) { //On ne devrait jamais tomber dans ce cas précis !!!
                            $messageErreur .= "Le document " . $document->titre . "(" . $document->id_document . ") est réservé.Les autres documents éventuels ont bien été restitués.<br /><br />";
                        } else {
                            $messageErreur .= "Le document " . $document->titre . "(" . $document->id_document . ") ne peut pas être rendu.<br /><br />";
                        }
                    } else {
                        $messageErreur .= "Document non trouvé<br />";
                    }
                } catch (Exception $e) {
                    echo $e;
                    $messageErreur .= "Document d'id: " . $document->id_document . " : erreur inconnue, Veuillez contacter votre administrateur.<br /><br />";
                }
            }
        } else {
            $messageErreur .= "Aucune référence précisée";
        }
        if ($messageErreur != "") {
            $this->afficheErreur($messageErreur);
            $this->footer();

        } else {
            $id_adherent = $adherent->id_adherent;
            $this->recapitulatifRetourEmprunt($list_document,$id_adherent );
            $this->footer();
        }
    }


    //Afficher le recap des docs rendus et ceux restants a rendre
    public function recapitulatifRetourEmprunt($list_document, $id_adherent)
    {
        try {
            $documentsRendus = Document::with("genre_doc", "type_doc","etat_doc", "pret")
                ->whereIn("document.id_document", $list_document)
                ->get();


          $documentsARendre = Adherent::getPretsEnCours($id_adherent);

            $this->app->render('recapitulatifRetours.twig', array(
                'documents_rendus' => $documentsRendus,
            'documents_restant_a_rendre' => $documentsARendre
            ));

        } catch (Exception $e) {
            $messageErreur = "Erreur inconnue, Veuillez contacter votre administrateur.<br>" . $e;
            $this->afficheErreur($messageErreur);

        }
    }
}
