﻿<?php

class Controller
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function accueil()
    {
        $this->affichageInterface();
    }

    public function affichageInterface()
    {
        $this->header();
        $this->sideBar();
    }

    public function header()
    {
        $isLogged = SecurityTools::isLogged();
        $admin = null;
        if ($isLogged === true) {
            $admin = Staff::find($_SESSION['admin']);
        }
        $this->app->render('header.twig', array(
            'isLogged' => $isLogged,
            'admin' => $admin
        ));
    }

    public function footer()
    {
        $this->app->render('footer.twig');
    }

    public function sideBar()
    {
        $urlRetourDocument = $this->app->urlFor('retourEmprunt');
        $this->app->render('sidebar.twig', array(
            'enregistrer_retour' => $urlRetourDocument
        ));
    }


    //Fonction qui affiche un message de succès
    //  Si le paramètre $bool n'est pas renseigné alors par défaut on affiche pas le header et la sideBar
    //  Si le paramètre $nbBack n'est pas renseigné alors par défaut le nombre
    //    de page de retour sera de 1
    public function afficheConfirmation($message = '', $bool = false, $nbBack = 1)
    {
        if($bool == true){
          $this->affichageInterface();
        }
        $this->app->render('info_action_confirme.twig', array(
          'message' => $message,
          'nbBack' => $nbBack
        ));
        if($bool == true){
          $this->footer();
        }
    }

    //Fonction qui affiche un message d'erreur
    //  Si le paramètre $bool n'est pas renseigné alors par défaut on affiche pas le header et la sideBar
    //  Si le paramètre $nbBack n'est pas renseigné alors par défaut le nombre
    //    de page de retour sera de 1
    public function afficheErreur($erreur = '', $bool = false, $nbBack = 1)
    {
        if($bool == true){
          $this->affichageInterface();
        }
        $this->app->render('info_erreur.twig', array(
          'erreur' => $erreur,
          'nbBack' => $nbBack
        ));
        if($bool == true){
          $this->footer();
        }
    }

    public function afficheLogin()
    {
        $this->affichageInterface();
        $this->app->render('affiche_login.twig');
        $this->footer();
    }

    public function login()
    {

        $email = $this->app->request->post('admin-email');
        $password = SecurityTools::SaltSha1Crypt($this->app->request->post('password'));
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $staff = Staff::where("email", "=", $email)->first();
            if ($staff != null && $password === $staff->mot_passe) {
                $_SESSION["admin"] = $staff->id_staff;
                $this->app->redirect($this->app->urlFor('retourEmprunt'));
            } else {
                $erreur = "Email ou mot de passe invalide";
            }
        } else {
            $erreur = "Format d'email invalide";
        }
        $this->affichageInterface();
        $this->afficheErreur($erreur);
        $this->footer();
        exit();
    }

    public function logout()
    {
        session_destroy();
        $this->app->redirect($this->app->urlFor('login'));
    }
}
