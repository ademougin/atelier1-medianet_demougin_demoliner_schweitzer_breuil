<?php

class ControllerAdherent extends Controller
{
    /*AFFICHAGE MENU GESTION*/
    public function gestionAdherent()
    {
        $this->affichageInterface();
        $ajoutAdherent = $this->app->urlFor("ajouterAdherent");
        $modifierAdherent = $this->app->urlFor("modifierAdherent");
        $supprimerAdherent = $this->app->urlFor("supprimerAdherent");
        $this->app->render('gestionAdherent.twig', array(
            'ajouterAdherent' => $ajoutAdherent,
            'modifierAdherent' => $modifierAdherent,
            'supprimerAdherent' => $supprimerAdherent
        ));
        $this->footer();
    }

    /*Ajout d'adhérent*/
    public function ajoutAdherent()
    {
        $this->affichageInterface();
        $action = $this->app->urlFor("enregistrerAdherent");
        $this->app->render('ajoutAdherent.twig', array(
            'action' => $action
        ));
        $this->footer();
    }


    public function enregistrerAdherent()
    {
        $this->affichageInterface();
        $nom = SecurityTools::securiseString($this->app->request->post('nom'));
        $prenom = SecurityTools::securiseString($this->app->request->post('prenom'));
        $email = SecurityTools::securiseString($this->app->request->post('mail'));
        $mot_passe = filter_var($this->app->request->post('mdp'), FILTER_SANITIZE_STRING);
        $num_tel = SecurityTools::securiseString($this->app->request->post('tel'));
        if (SecurityTools::IsNullOrEmptyString($nom) || SecurityTools::IsNullOrEmptyString($prenom) || SecurityTools::IsNullOrEmptyString($email) || (!filter_var($email, FILTER_VALIDATE_EMAIL)) || SecurityTools::IsNullOrEmptyString($mot_passe)) {
            $this->afficheErreur('Erreur de saisie, veuillez saisir tous les champs');
            $this->footer();
            exit;
        } else {
            $mot_passe = SecurityTools::SaltSha1Crypt($mot_passe);
            if ($adherent = Adherent::newAdherent($nom, $prenom, $email, $mot_passe, $num_tel) == 0) {
                $this->afficheConfirmation('Adhérent bien ajouté !', false, 2);
            } else {
                $this->afficheErreur('Erreur de sauvegarde');
            }
            $this->footer();
        }
    }


    /*RECHERCHE ET EDITION*/
    //formulaire
    public
    function rechercheAdherent()
    {
        $this->affichageInterface();
        $action = $this->app->urlFor("listeAdherent");
        $this->app->render('rechercheAdherent.twig', array(
            'action' => $action
        ));
        $this->footer();
    }

    //formulaire modif adhérent
    public
    function modifierAdherent($id)
    {
        try {
            $this->affichageInterface();
            $_SESSION['id'] = $id;
            $action = $this->app->urlFor("majAdherent");
            $actionSup = $this->app->urlFor("supprimerAdherent");
            $adherent = Adherent::find($id);
            $this->app->render('modifierAdherent.twig', array(
                'action' => $action,
                'actionSup' => $actionSup,
                'nom' => $adherent->nom,
                'prenom' => $adherent->prenom,
                'mail' => $adherent->email,
                'mdp' => $adherent->mot_passe,
                'tel' => $adherent->num_tel
            ));
        } catch (Exception $e) {
            $messageErreur = "Erreur inconnue, Veuillez contacter votre administrateur.";
            $this->afficheErreur($messageErreur);
        }
        $this->footer();

    }

    //Moteur mise a jour des informations de l'adhérent
    public
    function majAdherent()
    {
        try {
            $this->affichageInterface();
            if (isset($_SESSION['id'])) {
                $id = $_SESSION['id'];
                $adherent = Adherent::find($id);
                $adherent->nom = SecurityTools::securiseString($this->app->request->post('nom'));
                $adherent->prenom = SecurityTools::securiseString($this->app->request->post('prenom'));
                $adherent->email = SecurityTools::securiseString($this->app->request->post('mail'));
                $adherent->mot_passe = SecurityTools::SaltSha1Crypt(SecurityTools::securiseString($this->app->request->post('mdp')));
                if (SecurityTools::securiseString($this->app->request->post('tel')) != "") {
                    $adherent->num_tel = SecurityTools::securiseString($this->app->request->post('tel'));
                } else {
                    $adherent->num_tel = NULL;
                }
                $adherent->save();
                unset($_SESSION['id']);
                $this->afficheConfirmation('Modification enregistré', false, 2);
            } else {
                $this->afficheErreur('Erreur, recommencer la modification depuis le début ?', false, 2);
            }
        } catch (Exception $e) {
            $messageErreur = "Erreur inconnue, Veuillez contacter votre administrateur.";
            $this->afficheErreur($messageErreur);
        }
        $this->footer();
    }

    //affichage
    public
    function listeAdherent()
    {
        try {
            $this->affichageInterface();
            $nomSaisie = SecurityTools::securiseString($this->app->request->post('nom_adherent'));
            $mailSaisie = SecurityTools::securiseString($this->app->request->post('mail_adherent'));
            $numSaisie = SecurityTools::securiseString($this->app->request->post('num_adherent'));
            $action = $this->app->urlFor("modifierAdherent");
            $list_adherent = Adherent::where('nom', 'LIKE', '%' . $nomSaisie . '%')
                ->where('email', 'LIKE', '%' . $mailSaisie . '%')
                ->where('id_adherent', 'LIKE', '%' . $numSaisie . '%')
                ->get();
            $this->app->render('listeAdherent.twig', array(
                'action' => $action,
                'adherent_correspondant' => $list_adherent
            ));
        } catch (Exception $e) {
            $messageErreur = "Erreur inconnue, Veuillez contacter votre administrateur.";
            $this->afficheErreur($messageErreur);
        }
        $this->footer();
    }

    //Supression d'adhérent
    public
    function supressionAdherent()
    {
        try {
            $this->affichageInterface();
            if (isset($_SESSION['id'])) {
                $id = $_SESSION['id'];
                $adherent = Adherent::find($id);
                $nom = $adherent->nom;
                $prenom = $adherent->prenom;
                $mail = $adherent->email;
                $action = $this->app->urlFor("validerSuppressionAdherent");
                $this->app->render('confirmationSuppressionAdherent.twig', array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'mail' => $mail,
                    'action' => $action
                ));
            } else {
                $this->afficheErreur('Erreur', false, 3);
            }
        } catch (Exception $e) {
            $messageErreur = "Erreur inconnue, Veuillez contacter votre administrateur.";
            $this->afficheErreur($messageErreur);
        }
        $this->footer();
        

    }

    //confirmation de la suppression de l'adherent
    public
    function validerSuppressionAdherent()
    {
        $this->affichageInterface();
        if (isset($_SESSION['id'])) {

            $id = $_SESSION['id'];
            $pret = Pret::with('Document')
                ->where('id_adherent', '=', $id)
                ->get();

            if ($pret != null) {
                // var_dump($pret);
                // exit;
                foreach ($pret as $p) {

                    foreach ($p->document as $doc) {

                        $doc->pivot->delete();
                        $doc->id_etat_doc = 3;
                        $doc->save();

                    }
                    $p->delete();
                }

            }

            $reservation = Reservation::with('Document')
                ->where('id_adherent', '=', $id)
                ->delete();

            $adherent = Adherent::find($id);
            $adherent->delete();
            unset($_SESSION['id']);
            $this->afficheConfirmation('Adhérent supprimé', false, 4);
        } else {
            $this->afficheErreur('Erreur, cet adhérent n\'existe pas ou à peut-être déjà été supprimé...', false, 4);
        }
        $this->footer();
    }
}
