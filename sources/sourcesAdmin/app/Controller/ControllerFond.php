<?php

class ControllerFond extends Controller
{
    //page d'accueil de gestion des fonds
    public function gestionFond()
    {
        $this->affichageInterface();
        $this->app->render('gestionFond.twig');
        $this->footer();
    }


    //PAGES FORMULAIRE D AJOUT DE TYPE GENRE OU ETAT
    //formulaire
    public function ajouterTypeGenreEtat()
    {
        $this->affichageInterface();
        $this->app->render('ajouterTypeGenreEtat.twig');
        $this->footer();
    }

    //moteurs
    public function validerAjoutType()
    {
        $this->affichageInterface();
        $type = SecurityTools::securiseString($this->app->request->post('type'));
        if (SecurityTools::IsNullOrEmptyString($type)) {
            $this->afficheErreur("Veuillez entrer un nom.");
            $this->footer();
            exit;
        }
        if (count(Type_doc::where('nom_type', 'LIKE', $type)->get()) > 0) {
            $this->afficheErreur("Type déja existant");
            $this->footer();
            exit;
        } else {
            if (Type_doc::ajouter($type) == 0) {
                $this->afficheConfirmation("Enregistrement effectué : " . $type);
                $this->footer();
            } else {
                $this->afficheErreur("Erreur d'enregistrement");
                $this->footer();

            }
        }
    }

    public function validerAjoutGenre()
    {
        $this->affichageInterface();
        $genre = SecurityTools::securiseString($this->app->request->post('genre'));
        if (SecurityTools::IsNullOrEmptyString($genre)) {
            $this->afficheErreur("Veuillez entrer un nom.");
            $this->footer();
            exit;
        }
        if (count(Genre_doc::where('nom_genre', 'LIKE', $genre)->get()) > 0) {
            $this->afficheErreur("Genre déja existant");
            $this->footer();
            exit;
        } else {
            if (Genre_doc::ajouter($genre) == 0) {

                $this->afficheConfirmation("Enregistrement effectué : " . $genre);
                $this->footer();
            } else {

                $this->afficheErreur("Erreur d'enregistrement");
                $this->footer();
            }
        }
    }

    public function validerAjoutEtat()
    {
        {
            $this->affichageInterface();
            $etat = SecurityTools::securiseString($this->app->request->post('etat'));
            $raison = SecurityTools::securiseString($this->app->request->post('raison'));

            if (SecurityTools::IsNullOrEmptyString($etat)) {
                $this->afficheErreur("Veuillez entrer un nom.");
                $this->footer();
                exit;
            }
            if (count(Etat_doc::where('nom_etat', 'LIKE', $etat)->get()) > 0) {
                $this->afficheErreur("Etat déja existant");
                $this->footer();
                exit;
            } else {
                if (Etat_doc::ajouter($etat, $raison) == 0) {

                    $this->afficheConfirmation("Enregistrement effectué : " . $etat . " " . $raison);
                    $this->footer();
                } else {
                    $this->afficheErreur("Erreur d'enregistrement");
                    $this->footer();

                }
            }
        }
    }


    /*AFFICHER UN OU PLUSIEURS DOCUMENTS*/
    public function listeDocuments($documents)
    {
        $this->app->render('affichertableaudocuments.twig', array('documents' => $documents));
    }

    public function afficherDetailsDocument($id)
    {
        $this->affichageInterface();
        $_SESSION['id_doc'] = $id;
        $document = Document::getDetailsDocument($id);
        $prets = Pret_document::All();
        $etats = Etat_doc::All();
        $genres = Genre_doc::All();
        $types = Type_doc::All();
        $this->app->render('edition_doc.twig', array(
            'document' => $document,
            'prets' => $prets,
            'etats' => $etats,
            'genres' => $genres,
            'types' => $types));
    }


    /*RECHERCHER DOCUMENT*/
    //Formulaire
    public function moteurRechercheDocument()
    {
        $documents = Document::all();
        $this->app->render('moteurRechercheDocument.twig',
            array('documents' => $documents));
    }

    //moteur
    public function effectuerRechercheDocument()
    {
        //Lecture des champs
        $keyword = SecurityTools::securiseString($this->app->request->get('keyword'));
        $iddoc = SecurityTools::securiseInt($this->app->request->get('iddoc'));

        //Création de la requête
        $query = Document::orderBy('titre')
            ->with('etat_doc', 'type_doc', 'genre_doc');

        //si titre uniquement
        if ((strlen($keyword) > 0)) {
            if ($this->app->request->get('title_only')) {
                $query->where('titre', 'like', '%' . $keyword . '%');
            } //si titre + description
            else {
                $query->where(function ($query) use ($keyword) {
                    $query->where('titre', 'like', '%' . $keyword . '%');
                    $query->orWhere('description', 'like', '%' . $keyword . '%');
                });
            }
        }

        //par id document
        if ($iddoc > 0) {
            $query->where('id_document', '=', $iddoc);
        }

        //Execution de la requete
        $result = $query->get();

        //rendu
//        $this->affichageInterface();
        if (count($result) > 0) {
            $this->afficheResultatRecherche($result);
        } else {
            $this->affichageInterface();
            $this->afficheErreur("Aucun document trouvé");
            $this->footer();
        }
    }

    //rendu
    public function info_resultat_recherche()
    {
        $this->app->render('info_resultat_recherche.twig', array('' => ''));
    }

    public function afficheResultatRecherche($result)
    {
        $this->affichageInterface();
        $this->moteurRechercheDocument();
        //s'il y a des résultats
        if (count($result) > 0) {
            $this->info_resultat_recherche();
            $this->listeDocuments($result);
            $this->footer();
        } // si pas de résultats
        else {
            $this->app->render('info_erreur.twig', array('erreur' => "Aucun document trouvé."));
            $this->footer();
        }
    }


    /*SUPPRIMER UN DOCUMENT*/
    public function supprimerDocument($id)
    {
        $this->affichageInterface();

        try {
            $suppression = Document::supprDocument($id);
            if ($suppression < 0) {
                $message = "Erreur de suppression, veuillez contacter votre administrateur.";
                $this->afficheErreur($message, false, 2);
            } elseif ($suppression == 0) {
                $message = "Votre document a bien été supprimé";
                $this->afficheConfirmation($message, false, 3);
            } else {
                $message = "Votre document ne peut être supprimé car son état ne le permet pas.<br>Vous pouvez uniquement supprimer des documents disponibles ou perdus";
                $this->afficheErreur($message, false, 2);
            }
        } catch (Exception $e) {
            $message = "Erreur de suppression, veuillez contacter votre administrateur.";
            $this->afficheErreur($message, false, 1);
        }


        $this->footer();
    }


    /*AJOUTER UN DOCUMENT*/
    //Formulaire
    public function ajouterDocument()
    {
        $this->affichageInterface();
        $etats = Etat_doc::All();
        $genres = Genre_doc::All();
        $types = Type_doc::All();
        $this->app->render('ajouterDocument.twig', array(
            'etats' => $etats,
            'genres' => $genres,
            'types' => $types
        ));
        $this->footer();
    }

    //moteur
    public function validerAjoutDoc()
    {
        $this->affichageInterface();

        $titre = SecurityTools::securiseString($this->app->request->post('titre'));
        $description = SecurityTools::securiseString($this->app->request->post('description'));
        $urlimage = SecurityTools::securiseUrl($this->app->request->post('urlimage'));
        $type = $this->app->request->post('type_doc');
        $etat = $this->app->request->post('etat_doc');
        $genre = $this->app->request->post('genre_doc');

        if (SecurityTools::IsNullOrEmptyString($titre)) {
            $this->afficheErreur("Veuillez entrer un titre.");
            $this->footer();
            exit;
        }
        if (SecurityTools::IsNullOrEmptyString($description)) {
            $this->afficheErreur("Veuillez entrer une description.");
            $this->footer();
            exit;
        }

        if ($type <= 0 || $etat <= 0 || $genre <= 0) {
            $this->afficheErreur("Veuillez saisir tous les champs genre / type  /etat.");
            $this->footer();
            exit;
        }

        $id = Document::ajouter($titre, $description, $urlimage, $type, $etat, $genre);
        if ($id >= 0) {
            $this->app->render('info_document_ajoute.twig', array(
                'titre' => $titre,
                'id' => $id));
            $this->footer();
        } else {

            $this->afficheErreur("Erreur d'enregistrement");
            $this->footer();
        }

    }

    /*EDITER UN DOCUMENT*/
    //formulaire
    public function afficheRechercherEditerDocument()
    {
        $this->affichageInterface();
        $this->moteurRechercheDocument();
        $this->footer();
    }

    //moteur
    public function validerEditionDoc()
    {
        $this->affichageInterface();

        if (isset($_SESSION['id_doc'])) {
            $id = $_SESSION['id_doc'];
            $document = Document::find($id);

            //on récupère les variables
            $titre = SecurityTools::securiseString($this->app->request->post('titre'));
            $description = SecurityTools::securiseString($this->app->request->post('description'));
            $urlimage = SecurityTools::securiseUrl($this->app->request->post('urlimage'));
            $type = $this->app->request->post('type_doc');
            $etat = $this->app->request->post('etat_doc');
            $genre = $this->app->request->post('genre_doc');

            //on lance la mise a jour
            $iddoc = Document::miseajour($titre, $description, $urlimage, $type, $etat, $genre, $document->id_document);
            unset($_SESSION['id_doc']);

            //on affiche le résultat
            $message = "Le document " . $iddoc . " a bien été modifié";
            $this->afficheConfirmation($message, false, 3);
        } else {
            $this->afficheErreur('Erreur, recommencer la modification');
        }
    }

}
