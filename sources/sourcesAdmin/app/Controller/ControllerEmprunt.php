<?php

class ControllerEmprunt extends Controller
{
    //Module d'affichage de création d'emprunt
    public function afficheEmprunt()
    {
        $this->affichageInterface();
        $this->app->render('afficheremprunt.twig');
        $this->footer();
    }

    //Enregistre l'emprunt
    public function ajoutEmprunt()
    {
      try{
        $list_document = explode(';', $this->app->request->post('ref-document'));
        $email = SecurityTools::securiseEmail($this->app->request->post('email-client'));
        $refClient = SecurityTools::securiseInt($this->app->request->post('ref-client'));

        if ($email != null) {
            $client = Adherent::where('email', '=', $email)->first();
        }
        if ($refClient != null) {
            $client = Adherent::find($refClient);
        }
        if (isset($client) === true) {
            $id_client = $client->id_adherent;
        }

        if (isset($id_client) === true) {
            $pret = new Pret();
            $pret->id_adherent = $id_client;
            $pret->date_retour_limite = date('Y-m-d H:i:s', time() + RETOUR_LIMITE);
        } else {
            $this->affichageInterface();
            $this->afficheErreur("Adherent non trouvé");
            $this->footer();
            exit;
        }
        $pret->save();

        $documents_indisponible = array();
        $documents_dispo = array();
        foreach ($list_document as $id_document) {
            $id_doc = SecurityTools::securiseInt($id_document);
            $document = Document::find($id_doc);
            $reservation = Reservation::where("id_document", "=", $id_doc)->first();
            if ($document !== null) {
                if ($document->id_etat_doc == 1 ||
                    ($document->id_etat_doc == 5 && $client->id_adherent == $reservation->id_adherent))
                {
                    array_push($documents_dispo, $id_document);
                    $id_document = trim($id_document);
                    $pret_document = new Pret_document();
                    $pret_document->id_document = $id_document;
                    $pret_document->id_pret = $pret->id_pret;
                    $document->id_etat_doc = 2;
                    $document->save();
                    $pret_document->save();
                } else {
                    array_push($documents_indisponible, $id_document);
                }
            }
        }

        // Verifie si au moins un document est valide dans la liste renseigné
        if (sizeof($documents_dispo) === 0) {
//            $this->affichageInterface();
            $erreur = "Aucun document trouvé";
//            $this->footer();
        }
        // Verifie si tout les documents ne sont pas disponible
        if (sizeof($documents_indisponible) === sizeof($list_document)) {
//            $this->affichageInterface();
            $erreur = "Aucun document disponible à l'emprunt";
//            $this->footer();
        }
        if (isset($erreur) === true) {
            // Supprime le pret si la liste de reference document est invalide
            $pret->delete();
            $this->affichageInterface();
            $this->afficheErreur($erreur);
            $this->footer();
            exit();
        }


        $this->recapitulatifEmprunt($refClient, $documents_dispo, $documents_indisponible);
        exit;
      }catch(Exception $e){
        $messageErreur="Erreur inconnue, Veuillez contacter votre administrateur.";
        $this->afficheErreur($messageErreur);
      }
    }

    //Afficher le recap des docs empruntes
    public function recapitulatifEmprunt($id, $documents_dispo = null, $documents_indisponible = null)
    {
        $client = Adherent::find($id);
      try{
        $this->affichageInterface();
        $doc_indisponible = Document::with("etat_doc", "genre_doc", "type_doc")
            ->whereIn("id_document", $documents_indisponible)->get();

        $doc_dispo = Document::with("genre_doc", "type_doc", "pret")
            ->whereIn("id_document", $documents_dispo)->get();


        $this->app->render('recapitulatifEmprunt.twig', array(
            'adherent'=>$client,
            'documents_dispo' => $doc_dispo,
            'documents_indisponible' => $doc_indisponible
        ));
        $this->footer();
      }catch(Exception $e){
        $messageErreur="Erreur inconnue, Veuillez contacter votre administrateur.";
        $this->afficheErreur($messageErreur);
      }
    }
}
