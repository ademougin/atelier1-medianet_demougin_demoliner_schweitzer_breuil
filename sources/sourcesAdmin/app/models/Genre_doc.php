<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre_doc extends Eloquent
{
    protected $table = 'genre_doc';
    protected $primaryKey = 'id_genre_doc';
    public $timestamps = false;


    public function document(){
      return $this->hasMany('Document', 'id_document');
    }

    public static function ajouter($nom){
        try{
            $new = new Genre_doc();
            $new->nom_genre=$nom;
            $new->save();
            return 0;
        }
        catch (Exception $e){
            return 1;
        }
    }

}
