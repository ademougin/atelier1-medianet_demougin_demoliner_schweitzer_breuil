<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Adherent extends Eloquent
{
    protected $primaryKey = 'id_adherent';
    protected $table = 'adherent';
    public $timestamps = true;

    public function pret()
    {
        return $this->hasMany('Pret', 'id_adherent');
    }

    public function reservation()
    {
        return $this->hasMany('Reservation', 'id_adherent');
    }

    public static function getPretsEnCours($id)
    {
        $adherent = Adherent::with(array('pret.document' => function ($q3) {
            $q3->whereNull('date_retour_reelle');
        }))
        ->where('id_adherent', '=', $id)
        ->first();

        return $adherent;
    }

    public static function newAdherent($nom, $prenom, $email, $mot_passe, $num_tel)
    {
        try {
            $adherent = new Adherent;
            $adherent->nom = $prenom;
            $adherent->prenom = $prenom;
            $adherent->email = $email;
            $adherent->mot_passe = $mot_passe;
            $adherent->num_tel = $num_tel;
            $adherent->save();
            return 0;
        } catch (Exception $e) {
            return 1;
        }
    }
}
