<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Type_doc extends Eloquent
{
    protected $table = 'type_doc';
    protected $primaryKey = 'id_type_doc';
    public $timestamps = false;


    public function document(){
        return $this->hasMany('Document', 'id_document');
    }

    public static function ajouter($nom){
        try{
            $new = new Type_doc();
            $new->nom_type=$nom;
            $new->save();
            return 0;
        }
        catch (Exception $e){
            return 1;
        }
    }
}
