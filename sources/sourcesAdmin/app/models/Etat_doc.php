<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etat_doc extends Eloquent
{
    protected $table = 'etat_doc';
    protected $primaryKey = 'id_etat_doc';
    public $timestamps = false;

    public function document()
    {
        return $this->hasMany('document', 'id_etat_doc');
    }

    public static function ajouter($nom, $raison)
    {
        try {
            $new = new Etat_doc();
            $new->nom_etat = $nom;
//            if (isset($raison)) {
                $new->raison = $raison;
//            }
            $new->save();
            return 0;
        } catch (Exception $e) {
            return 1;
        }
    }
}
