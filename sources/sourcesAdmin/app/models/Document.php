<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Document extends Eloquent
{
    protected $table = 'document';
    protected $primaryKey = 'id_document';
    public $timestamps = true;

    public function etat_doc()
    {
        return $this->belongsTo('Etat_doc', 'id_etat_doc');
    }

    public function genre_doc()
    {
        return $this->belongsTo('Genre_doc', 'id_genre_doc');
    }

    public function type_doc()
    {
        return $this->belongsTo('Type_doc', 'id_type_doc');
    }

    public function pret()
    {
        return $this->belongsToMany('Pret', 'pret_document', 'id_document', 'id_pret')->withPivot('date_retour_reelle');
    }

    public function reservation()
    {
        return $this->hasMany('Reservation', 'id_document');
    }

    public static function ajouter($titre, $description, $urlimage, $type, $etat, $genre)
    {
        try {
            $new = new Document();
            $new->titre = $titre;
            $new->description = $description;
            $new->image = $urlimage;
            $new->id_type_doc = $type;
            $new->id_etat_doc = $etat;
            $new->id_genre_doc = $genre;
            $new->save();
            return $new->id_document;
        } catch (Exception $e) {
            return -1;
        }
    }

    //MAJ document
    public static function miseajour($titre, $description, $urlimage, $type, $etat, $genre, $iddoc)
    {
        try {
            $doc = Document::find($iddoc);
            $doc->titre = $titre;
            $doc->description = $description;
            $doc->image = $urlimage;
            $doc->id_type_doc = $type;
            $doc->id_etat_doc = $etat;
            $doc->id_genre_doc = $genre;
            $doc->save();
            return $doc->id_document;
        } catch (Exception $e) {
            return -1;
        }
    }

    //récupère tous les documents
    public static function getAllDocuments()
    {
        return Document::orderBy('created_at', 'DESC')
            ->with('etat_doc', 'genre_doc', 'type_doc')
            ->get();
    }

    //récupère le details d'un doc par son id
    public static function getDetailsDocument($id)
    {
        $doc = Document::with('etat_doc', 'genre_doc', 'type_doc', 'pret')
            ->find($id);
        return $doc;
    }

    //supprimer un document
    public static function supprDocument($id)
    {
        $doc = Document::with('etat_doc')->find($id);
        if ($doc->id_etat_doc == 1 || $doc->id_etat_doc == 3) {
            try {
                $document = Document::find($id);
                $prets = Pret_document::where('id_document', '=', $id);
                $reservations = Reservation::where('id_document', '=', $id);
                $reservations->delete();
                $prets->delete();
                $document->delete();
                return 0; //si suppression effectuée
            } catch (Exception $e) {
                echo $e;
                return -1; //si suppression exception
            }
        } else {
            return 1; //si conditions non remplies
        }
    }

    public static function getPretsEnCours($id)
    {
        $documents = Document::with('pret', 'pret.adherent')
            ->whereNull('pret_document.date_retour_reelle')
            ->join('pret', 'pret.id_adherent', '=', 'adherent.id_adherent')
            ->join('pret_document', 'pret_document.id_pret', '=', 'pret.id_pret')
            ->find($id);
        return $documents;
    }
}
