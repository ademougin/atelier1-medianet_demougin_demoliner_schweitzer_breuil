<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$tabConfig = parse_ini_file('config.ini');

$capsule = new Capsule;
$capsule->addConnection(array(
    'driver' => 'mysql',
    'host' => $tabConfig['host'],
    'database' => $tabConfig['database'],
    'username' => $tabConfig['username'],
    'password' => $tabConfig['password'],
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => ''
));
$capsule->setAsGlobal();
$capsule->bootEloquent();

?>
