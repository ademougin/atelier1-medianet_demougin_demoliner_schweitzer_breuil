-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 07 Novembre 2015 à 09:39
-- Version du serveur :  5.1.73
-- Version de PHP :  5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `breuil1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
CREATE TABLE IF NOT EXISTS `adherent` (
  `id_adherent` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mot_passe` varchar(100) NOT NULL,
  `num_tel` varchar(10) DEFAULT NULL,
  `nb_retard` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_adherent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `id_document` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `commentaire` varchar(100) DEFAULT NULL,
  `id_etat_doc` int(10) unsigned NOT NULL,
  `id_genre_doc` int(10) unsigned NOT NULL,
  `id_type_doc` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`),
  KEY `fk_document_etat_doc_id_etat_doc_idx` (`id_etat_doc`),
  KEY `fk_document_genre_doc_id_genre_doc_idx` (`id_genre_doc`),
  KEY `fk_document_type_doc_id_type_doc_idx` (`id_type_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `document`
--

INSERT INTO `document` (`id_document`, `titre`, `description`, `image`, `commentaire`, `id_etat_doc`, `id_genre_doc`, `id_type_doc`, `created_at`, `updated_at`) VALUES
(1, 'AC/DC', 'De la musique qui déchire, du bon rock fort', 'http://www.franceinfo.fr/sites/default/files/asset/images/2015/06/acdc.jpg', NULL, 1, 7, 2, '2015-11-03 08:15:55', '2015-11-06 20:34:10'),
(2, 'La face cachée de margo', 'Mar-go-Roth-Spie-gel-man, le nom aux six syllabes qui fait fantasmer Quentin depuis toujours. Alors forcément, quand elle s''introduit dans sa chambre, une nuit, par la fenêtre ouverte, pour l''entraîner dans une expédition vengeresse, il la suit.Mais au lendemain de leur folle nuit blanche, Margo ne se présente pas au lycée, elle a disparu.Quentin saura-t-il décrypter les indices qu''elle lui a laissés pour la retrouver ? Plus il s''en approche, plus Margo semble lui échapper...', 'http://static.fnac-static.com/multimedia/Images/FR/NR/b7/5d/5a/5922231/1507-1.jpg', NULL, 1, 3, 1, '2015-11-01 08:15:55', '2015-11-06 20:34:10'),
(3, 'Moi, moche et méchant 2', 'Ayant abandonné la super-criminalité et mis de côté ses activités funestes pour se consacrer à la paternité et élever Margo, Édith et Agnès, Gru, et avec lui, le Professeur Néfario et les Minions, doivent se trouver de nouvelles occupations. Alors qu’il commence à peine à s’adapter à sa nouvelle vie tranquille de père de famille, une organisation ultrasecrète, menant une lutte acharnée contre le Mal à l’échelle planétaire, vient frapper à sa porte. Soudain, c’est à Gru, et à sa nouvelle coéquipière Lucy, que revient la responsabilité de résoudre une série de méfaits spectaculaires. Après tout, qui mieux que l’ex plus méchant méchant de tous les temps, pourrait attraper celui qui rivalise pour lui voler la place qu’il occupait encore récemment.', 'http://www.avoir-alire.com/IMG/jpg/mmm2dvd.jpg', NULL, 1, 4, 3, '2015-11-02 08:15:55', '2015-11-07 08:25:22'),
(4, 'Star Wars: La Trilogie', 'LA trilogie spatiale de référence ! Les aventures de Luke Skywalker tandis qu&#39;il fait l&#39;apprentissage de la Force pour devenir un chevalier Jedi comme son père qui fut terrassé jadis par le côté obscur...', 'http://merome.net/monnaiem/images/produits/20130623125435_star-wars-trilogie.jpg', NULL, 1, 5, 3, '2015-10-03 07:15:55', '2015-11-07 08:27:25'),
(5, 'Martine à la ferme', 'Martine rend visite à la ferme de son cousin Jean-Pierre. C’est l’occasion pour elle de découvrir tous ses animaux : les poussins, les lapins, le petit mouton, les oies, le veau et le poulain, sans oublier Moustache, le chat, et Médor, le chien, Martine se fait plein de nouveaux amis…', 'http://www.bedetheque.com/media/Couvertures/Couv_134758.jpg', NULL, 1, 6, 1, '2015-10-02 07:15:55', '2015-11-06 20:44:43'),
(6, 'A fleur 2 Peau', 'CD de Tragédie', 'http://pmcdn.priceminister.com/photo/Tragedie-A-Fleur-2-Peau-CD-Album-139292730_ML.jpg', NULL, 1, 8, 2, '2015-11-03 08:15:55', '2015-11-06 20:19:21'),
(8, 'Fury', 'Avril 1945, alors que la Seconde Guerre mondiale touche à sa fin, le sergent Don « Wardaddy » Collier commande un char M4 Sherman et son équipage de cinq soldats de la 2e division blindée américaine pour une mission risquée derrière les lignes ennemies durant la campagne d Allemagne.', 'http://fr.web.img5.acsta.net/pictures/14/09/22/16/44/411457.jpg', NULL, 1, 10, 3, '2015-11-06 19:21:14', '2015-11-07 07:33:01'),
(9, 'La Lignée', 'Un Boeing 777 se pose à l&#39;Aéroport international John-F.-Kennedy de New York sans qu&#39;aucun signe de vie n&#39;émane de l&#39;appareil. Le Docteur Ephraïm « Eph » Goodweather et son équipe découvrent alors que tous les passagers ont été contaminés par un virus qui les a transformés en vampires.', 'http://www.images-booknode.com/book_cover/95/la-lignee,-tome-1---la-lignee-95318-250-400.jpg', NULL, 1, 2, 1, '2015-11-06 19:22:05', '2015-11-07 08:18:43'),
(10, 'M - Labo M', 'Labo M est un album studio de Matthieu Chedid, sorti en 2003. Cet album instrumental se veut plus intimiste que les précédents (on l&#39;y entend murmurer sur certaines chansons). Alternant chansons rock et atmosphère calme avec violoncelles et guitare classique (Les Saules), -M- propose des morceaux de chansons dans son studio (appelé le Labo M). Quelques versions originelles de certaines chansons présentes sur d&#39;autres albums y figurent également.  Une sorte de suite, initialement intitulée Labo M 2, est attendue pour novembre 2015. Sous le titre La B.O² -M-, il prend la forme d&#39;un livre-cd avec le dessinateur Matthias Picard (Éditions 2024).', 'https://ssl7.ovh.net/~frisethe/img/albums/original/546/546.jpg', NULL, 1, 9, 2, '2015-11-06 19:24:48', '2015-11-07 08:22:32'),
(11, 'Train - Save me San Francisco', 'Album de Train datant de 2009 - 1.Save me, San Francisco - 2.Hey, Soul Sister - 3. I Got You - 4. Parachute - 5. This Ain t Goodbye - 6. If It s Love - 7. You Already Know - 8. Words - 9. Brick by Brick - 10.Breakfast in Bed - 11. Marry Me', 'http://ecx.images-amazon.com/images/I/71F1ASAQOWL._SX522_PJautoripRedesignedBadge,TopRight,0,-35_OU11__.jpg', NULL, 1, 3, 2, '2015-11-06 19:48:15', '2015-11-07 08:27:11'),
(12, 'L&#39;oreille cassée', 'L&#39;oreille cassée; (1937) est une course poursuite palpitante. Afin de récupérer un fétiche volé, Tintin s&#39;embarque pour l&#39;Amérique du Sud où s&#39;opposent toutes sortes d&#39;intérêts : militaires, économiques, financiers. La guerre décrite dans l album, entre le San Theodoros et le Nuevo Rico, est en fait la transposition de la guerre du Gran Chaco qui venait d&#39;opposer, trois ans durant, la Bolivie et la Paraguay.', 'http://bd.casterman.com/docs/Albums/4738/9782203001053_cb.jpg', NULL, 1, 11, 1, '2015-11-06 19:50:29', '2015-11-07 08:17:20'),
(14, 'Tintin au tibet', 'Un avion de ligne à bord duquel le jeune Chinois Tchang se rendait en Europe s&#39;est écrasé dans l&#39;Himalaya. Tintin au Tibet (1960), pure histoire d&#39;amitié, sans le moindre méchant, décrit la recherche désespérée à laquelle Tintin se livre pour retrouver son ami. Ce récit pathétique, qui rompt avec le ton extraverti des épisodes précédents, démontre que la fidélité et l&#39;espoir sont capables de vaincre tous les obstacles, et que les préjugés - en l&#39;occurrence, à l&#39;égard de l&#39;abominable homme des neiges - sont bien souvent le fruit de l&#39;ignorance.', 'http://bd.casterman.com/docs/Albums/4724/9782203001190_cb.jpg', NULL, 1, 11, 1, '2015-11-06 19:55:58', '2015-11-07 08:30:47'),
(15, 'BoJack Horseman', 'BoJack Horseman est une sitcom animée américaine créée par Raphael Bob-Waksberg (en). Will Arnett prête sa voix à BoJack Horseman, le personnage principal. Le casting inclut aussi Amy Sedaris, Alison Brie, Paul F. Tompkins et Aaron Paul en guest-star. La première saison a été mise en ligne le 22 août 2014, sur Netflix. Après des critiques très mitigées, la série a su prouver sa popularité auprès des internautes et, après moins d&#39;une semaine en ligne, une nouvelle saison est annoncée par Netflix pour 2015.', 'http://www.cynicaldandy.com/wp-content/uploads/2014/11/bojack-horseman-2.jpg', NULL, 1, 4, 3, '2015-11-06 19:58:33', '2015-11-07 08:12:32'),
(16, 'Walker, Texas Ranger', 'La seule chose qui arrive à la cheville de Chuck Norris... c&#39;est sa chaussette.', 'http://www.gtamodding.fr/download/armes/1431723369/cover.jpg', NULL, 1, 10, 3, '2015-11-06 19:59:10', '2015-11-07 08:31:19'),
(17, 'Friends', 'Monica Geller, une jeune cuisinière d&#39;environ 25 ans, vit dans un appartement situé à Manhattan, dans le Greenwich Village. Un jour, son amie d&#39;enfance, Rachel Green, qu&#39;elle n&#39;avait plus vue depuis des années, lui rend visite après avoir rompu avec son fiancé le jour de leur mariage. Rachel devient alors la nouvelle colocataire de Monica et s’intègre sans problème à son groupe d amis, composé de : Phoebe Buffay (l&#39;ancienne colocataire de Monica), Ross Geller (le frère de Monica, qui est secrètement amoureux de Rachel depuis le lycée), Chandler Bing (qui est le meilleur ami de Ross depuis l&#39;université) et Joey Tribbiani (le colocataire actuel de Chandler). Ces deux derniers vivent dans l&#39;appartement juste en face de celui de Monica, sur le même palier.  La série raconte la vie quotidienne de ces six amis, ainsi que l&#39;évolution de leur vie professionnelle et affective pendant dix ans.', 'http://highlights.com.tn/wp-content/uploads/2015/08/friends-complet.jpg', NULL, 1, 4, 3, '2015-11-06 20:00:03', '2015-11-07 08:14:11'),
(18, 'Le Ukulélé pour les Nuls', 'Originaire des îles Hawaï, star des orchestres improvisés sur la plage et tout de bois vêtu, je suis ? Je suis ? Le ukulélé !  Cet instrument, qui ressemble pourtant fortement à une guitare miniature, nécessite un apprentissage tout particulier : une position de jeu, des cordes jouées un peu en dessous de la jonction du manche et du corps, la possibilité de pincer les cordes du pouce, de l&#39;index et du majeur (le finger picking ) sont quelques exemples des notions indispensables avec lesquelles vous devrez vous familiariser pour maîtriser tout son potentiel.  Avec la méthode Nuls, choisissez le ukulélé qui vous convient, apprenez les notions de base et devenez à l aise à l heure de jouer une berceuse hawaïenne, une partition pop ou folk, ou même un standard de Noël ! Offert avec le livre, 1 CD audio d&#39;une heure pour s&#39;entraîner sur des pistes enregistrées.', 'http://www.pourlesnuls.fr/images_livres/gf/9782754043007.gif', NULL, 1, 12, 1, '2015-11-06 20:06:05', '2015-11-07 08:19:32'),
(20, 'Daft Punk - Around the world', 'maxi vinyle des Daft Punk (2011)', 'http://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/8/6/7/0724389411768.jpg', NULL, 1, 14, 5, '2015-11-07 07:48:30', '2015-11-07 07:52:46'),
(21, 'Led Zeppelin IV', 'Led Zeppelin IV est le nom généralement utilisé mais non officiel pour nommer le quatrième album du groupe Led Zeppelin sorti le 8 novembre 1971. Il s&#39;agit d&#39;un des albums les plus vendus de l&#39;histoire, avec plus de 23 millions de copies écoulées aux seuls États-Unis et 37 millions d&#39;exemplaires vendus dans le monde. L&#39;album fut composé et mixé aux Basing Street Studios d&#39;Island Records, à Londres, à Headley Grange, une demeure victorienne isolée dans l&#39;East Hampshire, et à Sunset Sound (Los Angeles).', 'https://upload.wikimedia.org/wikipedia/en/2/26/Led_Zeppelin_-_Led_Zeppelin_IV.jpg', NULL, 1, 13, 2, '2015-11-07 07:48:49', '2015-11-07 08:21:42'),
(22, 'Dorothée - Hou ! La Menteuse / La valise', 'Hou ! La menteuse est le deuxième album studio de Dorothée, sorti en avril 1982. Il s&#39;agit du plus gros succès commercial de sa carrière.  La chanson Hou la menteuse ! se classe no 1 des meilleures ventes de singles IFOP pendant 9 semaines, s&#39;écoulant à 1 300 000 exemplaires.  Cet opus contient quelques particularités qui deviendront récurrentes sur tous les albums de la chanteuse : la présence du parolier Michel Jourdan, le nombre de chansons (14 titres) et la présence de La valise. Cette chanson devait à l&#39;origine être éditée en single mais ne figurera finalement qu&#39;en face B du 45 tours de Hou ! La menteuse. Elle deviendra toutefois un incontournable de son répertoire : 16 chansons dérivées de La valise seront enregistrées.  La pochette de l&#39;album est illustrée par des dessins de Cabu, qui participe à cette époque à l&#39;émission Récré A2 présentée par Dorothée.', 'http://ecx.images-amazon.com/images/I/51M47jYHz7L._AA160_.jpg', NULL, 1, 15, 5, '2015-11-07 07:51:45', '2015-11-07 08:16:09'),
(23, 'Pink Floyd - The Dark Side of the Moon', 'The Dark Side of the Moon2 est le huitième album studio du groupe de rock progressif britannique Pink Floyd. Paru le 10 mars 1973 aux États-Unis et le 23 mars 1973 au Royaume-Uni, il est souvent considéré comme leur album le plus abouti. Il aborde des thèmes universels, comme le travail, l&#39;argent, la vieillesse, la guerre, la folie et la mort.', 'https://upload.wikimedia.org/wikipedia/en/3/3b/Dark_Side_of_the_Moon.png', NULL, 1, 16, 2, '2015-11-07 07:51:51', '2015-11-07 08:25:50'),
(24, 'Le voyage de Chihiro', 'Un couple et leur jeune fille, Chihiro, roulent vers leur nouvelle maison, la petite regrettant ses anciens amis et son ancienne école à la suite de ce déménagement. Ils s&#39;égarent en chemin et arrivent devant un mystérieux tunnel qui effraie Chihiro mais intrigue ses parents. En le traversant à pied, ils se retrouvent, tous les trois, sur des collines en bordure de mer et découvrent ce qu ils pensent être un ancien parc à thème abandonné.  Envoûtés par les odeurs de nourriture appétissantes du parc, les parents de Chihiro s&#39;installent et dégustent les plats délicieux d&#39;un restaurant au personnel absent pendant que leur fille explore la ville. Lorsque soudainement la nuit tombe, des ombres étranges apparaissent un peu partout et se mettent à animer les rues.', 'http://fr.web.img1.acsta.net/medias/nmedia/00/02/36/71/chihiro.jpg', NULL, 1, 6, 3, '2015-11-07 07:55:24', '2015-11-07 08:20:54'),
(25, 'The Holiday', 'Réalisé par Nancy Meyers Avec Cameron Diaz, Kate Winslet, Jude Law. Une Américaine (Amanda) et une Anglaise (Iris), toutes deux déçues des hommes, décident, sans se connaître, d&#39;échanger leurs appartements. Iris, va débarquer dans une demeure de rêve tandis que la distinguée Amanda découvre une petite maison de campagne sans prétentions. Les deux femmes pensent passer de paisibles vacances loin de la gent masculine, mais c&#39;est sans compter sur l&#39;arrivée inopinée de l&#39;éditeur Graham (Jude Law) - le frère d&#39;Iris - dans la retraite de l&#39;Américaine et la rencontre tout aussi fortuite entre le compositeur Miles (Jack Black) et Iris...', 'http://www.gstatic.com/tv/thumb/movieposters/162526/p162526_p_v7_aa.jpg', NULL, 1, 3, 3, '2015-11-07 07:56:24', '2015-11-07 08:30:06'),
(26, 'Queen Of The Stone Age - Songs for the Deaf', 'À sa sortie le 27 août 2002, Songs for the Deaf est largement reconnu par la critique comme le meilleur album des QOTSA à ce jour, permettant au groupe de gagner son premier disque d&#39;or aux États-Unis. Songs for the Deaf est un album-concept, emmenant la personne du désert des Mojaves à Los Angeles, en écoutant des bribes de stations de radio. Le magazine Entertainment Weekly écrit : « c&#39;est le meilleur album de hard-rock de l&#39;année », Splendid ajoute : « Songs for the Deaf est un nouveau genre, aussi dur que le titane, semblable à une démolition. Ce n&#39;est pas le heavy metal de votre père. C&#39;est mieux. ».', 'https://upload.wikimedia.org/wikipedia/en/0/01/Queens_of_the_Stone_Age_-_Songs_for_the_Deaf.png', NULL, 1, 17, 2, '2015-11-07 07:57:07', '2015-11-07 08:26:34'),
(27, 'Mary à tout prix', 'Ted décide de retrouver son amour de jeunesse, la belle Mary. Pour ce faire, il engage un détective privé, Pat Healy. Sûr d&#39;avoir affaire à un fou, Healy décide de rejoindre Mary à Miami. Elle est belle, gentille, s&#39;occupe d&#39;enfants handicapés et elle est célibataire. Déterminé à garder Mary pour lui, il ment à Ted à propos de Mary : il lui raconte qu&#39;elle est devenue laide, qu&#39;elle est mariée et a 4 enfants. Tucker, un jeune architecte, est lui aussi amoureux de Mary. Il décide de s&#39;allier avec Healy pour évincer Ted, qui est lui aussi venu à Miami.', 'http://a5.mzstatic.com/eu/r30/Video/v4/73/70/39/73703912-4c2c-ac9f-5b72-e61d657459d9/poster227x227.jpeg', NULL, 1, 4, 3, '2015-11-07 07:57:12', '2015-11-07 08:23:26'),
(28, 'Michael Jackson - Thriller', 'Thriller est le sixième album studio de l&#39;artiste américain Michael Jackson. Coproduit par Quincy Jones, il sort le 30 novembre 1982 chez Epic Records, à la suite du succès commercial et critique de l&#39;album Off the Wall (1979). Thriller explore des genres similaires à ceux d&#39;Off the Wall, tels le funk, le post-disco, la musique soul, le soft rock, le R&B et la pop. Toutefois, les paroles de Thriller traitent généralement de thèmes plus sombres, comme la paranoïa et le surnaturel.', 'https://upload.wikimedia.org/wikipedia/en/5/55/Michael_Jackson_-_Thriller.png', NULL, 1, 8, 5, '2015-11-07 07:58:51', '2015-11-07 08:23:53');

-- --------------------------------------------------------

--
-- Structure de la table `etat_doc`
--

DROP TABLE IF EXISTS `etat_doc`;
CREATE TABLE IF NOT EXISTS `etat_doc` (
  `id_etat_doc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom_etat` varchar(45) NOT NULL,
  `raison` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_etat_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `etat_doc`
--

INSERT INTO `etat_doc` (`id_etat_doc`, `nom_etat`, `raison`) VALUES
(1, 'disponible', NULL),
(2, 'emprunté', NULL),
(3, 'indisponible', 'perdu'),
(4, 'indisponible', 'en réparation'),
(5, 'indisponible', 'réservé'),
(6, 'emprunté et reservé', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `genre_doc`
--

DROP TABLE IF EXISTS `genre_doc`;
CREATE TABLE IF NOT EXISTS `genre_doc` (
  `id_genre_doc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom_genre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_genre_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `genre_doc`
--

INSERT INTO `genre_doc` (`id_genre_doc`, `nom_genre`) VALUES
(1, 'Policier'),
(2, 'Thriller'),
(3, 'Romance'),
(4, 'Humour'),
(5, 'Science fiction'),
(6, 'Jeunesse'),
(7, 'Hard-Rock'),
(8, 'RnB'),
(9, 'Rock français'),
(10, 'Guerre'),
(11, 'Aventure'),
(12, 'Culture'),
(13, 'Rock'),
(14, 'Electro'),
(15, 'Variété'),
(16, 'Rock Progressif'),
(17, 'Stoner Rock');

-- --------------------------------------------------------

--
-- Structure de la table `pret`
--

DROP TABLE IF EXISTS `pret`;
CREATE TABLE IF NOT EXISTS `pret` (
  `id_pret` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_adherent` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_retour_limite` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_pret`),
  KEY `fk_pret_adherent_id_adherent_idx` (`id_adherent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Structure de la table `pret_document`
--

DROP TABLE IF EXISTS `pret_document`;
CREATE TABLE IF NOT EXISTS `pret_document` (
  `id_pret_document` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_document` int(10) unsigned NOT NULL,
  `id_pret` int(10) unsigned NOT NULL,
  `date_retour_reelle` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pret_document`),
  KEY `fk_pret_document_pret_id_pret_idx` (`id_pret`),
  KEY `fk_pret_document_document_id_document` (`id_document`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id_reservation` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_adherent` int(10) unsigned NOT NULL,
  `id_document` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_reservation`),
  KEY `fk_reservation_adherent_id_adherent_idx` (`id_adherent`),
  KEY `fk_reservation_document_id_document_idx` (`id_document`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `id_staff` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mot_passe` varchar(100) NOT NULL,
  `num_tel` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_staff`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `staff`
--

INSERT INTO `staff` (`id_staff`, `nom`, `prenom`, `email`, `mot_passe`, `num_tel`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin ', 'admin@mail.com', 'a78503b76a8d0f4e448962f0456e92df382d9586', '0000000000', '2015-11-07 08:36:00', '0000-00-00 00:00:00'),
(2, 'De Moliner', 'Clara', 'clara@mail.com', '46bcd7ae58e4d446f8568743e57455975c506e84', NULL, '2015-11-03 08:02:21', '2015-11-03 08:02:21'),
(3, 'Schweitzer', 'Victorien', 'victorien@mail.com', '958e69ea5cb3e54664c41d7b53f86c10d73b9cd4', '0686620488', '2015-11-03 08:03:45', '2015-11-03 08:03:45'),
(4, 'Breuil', 'Fabien', 'fabien@mail.com', 'fad91a2132fd7c764ba47db06cb33dc66d6923be', '0698630514', '2015-11-03 08:03:45', '2015-11-03 08:03:45'),
(5, 'Demougin', 'Alban', 'alban@mail.com', '571006545907244e36eab4152a8386fb2a522c0a', NULL, '2015-11-03 08:02:21', '2015-11-03 08:02:21');

-- --------------------------------------------------------

--
-- Structure de la table `type_doc`
--

DROP TABLE IF EXISTS `type_doc`;
CREATE TABLE IF NOT EXISTS `type_doc` (
  `id_type_doc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id_type_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `type_doc`
--

INSERT INTO `type_doc` (`id_type_doc`, `nom_type`) VALUES
(1, 'livre'),
(2, 'CD'),
(3, 'DVD'),
(4, 'e-book'),
(5, 'Vinyle');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `fk_document_etat_doc_id_etat_doc` FOREIGN KEY (`id_etat_doc`) REFERENCES `etat_doc` (`id_etat_doc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_document_genre_doc_id_genre_doc` FOREIGN KEY (`id_genre_doc`) REFERENCES `genre_doc` (`id_genre_doc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_document_type_doc_id_type_doc` FOREIGN KEY (`id_type_doc`) REFERENCES `type_doc` (`id_type_doc`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pret`
--
ALTER TABLE `pret`
  ADD CONSTRAINT `fk_pret_adherent_id_adherent` FOREIGN KEY (`id_adherent`) REFERENCES `adherent` (`id_adherent`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pret_document`
--
ALTER TABLE `pret_document`
  ADD CONSTRAINT `fk_pret_document_document_id_document` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pret_document_pret_id_pret` FOREIGN KEY (`id_pret`) REFERENCES `pret` (`id_pret`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_reservation_adherent_id_adherent` FOREIGN KEY (`id_adherent`) REFERENCES `adherent` (`id_adherent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reservation_document_id_document` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
